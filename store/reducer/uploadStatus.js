import { ADD_IMAGE, REMOVE_IMAGE } from "../action/uploadStatus";
import UploadedImg from "../../models/capturedImages";

const initialState = {
  photos: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_IMAGE:
      // const newOrder new Order
      const newImg = new UploadedImg(
        // new Date().toString(),
        Math.random().toString(),
        action.imageData.title,
        action.imageData.place,
        action.imageData.image
      );

      return {
        ...state,
        photos: state.photos.concat(newImg),
      };
    case REMOVE_IMAGE:
      //   return {
      //     ...state,
      //     photos: state.photos.concat(newImg),
      //   };
      debugger;
      return {
        ...state,
        photos: state.photos.filter((data) => data.id != action.imgId),
      };
  }

  return state;
};
