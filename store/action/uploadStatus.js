export const ADD_IMAGE = "ADD_IMAGE";
export const REMOVE_IMAGE = "REMOVE_IMAGE";
import * as FileSystem from "expo-file-system";
import { Alert } from "react-native";

export const addImage = (title, place, selectedImage) => {
  return async (dispatch) => {
    // const fileName = selectedImage.split("/").pop();
    // const newPath = FileSystem.documentDirectory(fileName);
    // try {
    //   await FileSystem.moveAsync({
    //     from: selectedImage,
    //     to: newPath,
    //   });
    // } catch (error) {
    //   console.log(error);
    //   throw error;
    // }

    let reqPayload = {
      auth: {
        api_key: "d6e4e8894e07267916d49c4d8f681b17",
        api_secret: "8342305f1b5df51abcbb2214890b1cc29ec7c09a",
      },
      url: selectedImage,
      wait: true,
    };
    debugger;
    const resp = await fetch("https://kraken.io/docs/upload-url", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: reqPayload,
    });

    debugger;

    // url = "https://kraken.io/docs/upload-url";
    // try {
    //   let reqPayload = {
    //     auth: {
    //       api_key: "d6e4e8894e07267916d49c4d8f681b17",
    //       api_secret: "8342305f1b5df51abcbb2214890b1cc29ec7c09a",
    //     },
    //     url: selectedImage,
    //     wait: true,
    //   };
    //   debugger;
    //   const resp = await fetch("https://kraken.io/docs/upload-url", {
    // method: "POST",
    // headers: {
    //   "Content-Type": "application/json",
    // },
    //     body: JSON.stringify(reqPayload),
    //   });
    //   debugger;
    //   const resData = await response.json();
    //   dispatch({
    //     type: ADD_IMAGE,
    //     imageData: { title: title, place: place, image: selectedImage },
    //   });
    //   if (!response.ok) {
    //     throw new Error("Something went wrong!");
    //   }
    // } catch (error) {
    //   Alert.alert(error);
    // }
    dispatch({
      type: ADD_IMAGE,
      imageData: { title: title, place: place, image: selectedImage },
    });
  };
};

export const removeImage = (imageId) => {
  debugger;
  return { type: REMOVE_IMAGE, imgId: imageId };
};
