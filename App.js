import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createStore, combineReducers, applyMiddleware } from "redux";
import { AppLoading } from "expo";
import ReduxThunk from "redux-thunk";
import { Provider } from "react-redux";
import MyNavigator from "./navigation/MyNavigator";
import uploadImgData from "./store/reducer/uploadStatus";
import { composeWithDevTools } from "redux-devtools-extension";

const rootReducer = combineReducers({
  imagesData: uploadImgData,
});

const store = createStore(
  rootReducer,

  composeWithDevTools(applyMiddleware(ReduxThunk))
);

export default function App() {
  return (
    // <>
    <Provider store={store}>
      <MyNavigator />
    </Provider>
    // </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
