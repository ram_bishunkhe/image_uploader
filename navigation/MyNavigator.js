import React from "react";
import Colors from "../assets/Constants/Colors";
import { createDrawerNavigator } from "react-navigation-drawer";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import createAnimatedSwitchNavigator from "react-navigation-animated-switch";
import { Transition } from "react-native-reanimated";
import Home from "../screens/Kraken/Home";
import CaptureImage from "../screens/Kraken/CaptureImage";
import UploadStatus from "../screens/Kraken/UploadStatus";

const defaultNavOptions = {
  headerStyle: {
    backgroundColor: Colors.primaryColor,

    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    // fontFamily: "open-sans-bold",
    fontWeight: "bold",
  },
  headerBackTitleStyle: {
    // fontFamily: "open-sans",
  },
};

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    Capture: CaptureImage,
    ImageList: UploadStatus,
    // Home: {
    //   screen: Home,
    // },
  },
  {
    defaultNavigationOptions: defaultNavOptions,
  }
);

export default createAppContainer(AppNavigator);
