import React from "react";
import { View, Text, Button } from "react-native";
import Colors from "../../assets/Constants/Colors";

const Home = (props) => {
  return (
    <View>
      <Text>App Description</Text>
      <View style={{ width: 150, cursor: "pointer" }}>
        <Button
          color={Colors.primaryColor}
          title="Capture Image"
          onPress={() => props.navigation.navigate("Capture")}
        />
      </View>
    </View>
  );
};

Home.navigationOptions = (navigationData) => {
  return {
    headerTitle: "Home",
  };
};

export default Home;
