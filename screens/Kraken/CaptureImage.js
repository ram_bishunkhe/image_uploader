import React, { useState, useEffect } from "react";
import {
  View,
  Button,
  Text,
  StyleSheet,
  Image,
  Alert,
  TextInput,
  ScrollView,
} from "react-native";
import { useDispatch } from "react-redux";
import Colors from "../../assets/Constants/Colors";
import UploadStatus from "./UploadStatus";

import * as imageAction from "../../store/action/uploadStatus";

import ImagePicker from "../../components/ImagePicker";

const CaptureImage = (props) => {
  const dispatch = useDispatch();
  const [titleValue, setTitleValue] = useState("");
  const [placeValue, setPlaceValue] = useState("");
  const [selectedImage, setSelectedImage] = useState();
  const titleChangeHandler = (text) => {
    setTitleValue(text);
  };
  const placeHandler = (place) => {
    setPlaceValue(place);
  };

  const imageTakenHandler = async (imagePath) => {
    debugger;
    console.log(imagePath);
    setSelectedImage(imagePath);
  };

  const saveImageHandler = async () => {
    console.log(titleValue, selectedImage);

    await dispatch(imageAction.addImage(titleValue, placeValue, selectedImage));
    setTitleValue("");
    setPlaceValue();
    setSelectedImage();
    // props.navigation.goBack();
  };

  useEffect(() => {
    setTitleValue("");
    setSelectedImage();
    return () => {
      console.log("cleared");
    };
  }, []);

  return (
    <ScrollView>
      <View style={styles.form}>
        <Text style={styles.label}>Title</Text>
        <TextInput
          style={styles.textInput}
          onChangeText={titleChangeHandler}
          value={titleValue}
        />
        <Text style={styles.label}>Place</Text>
        <TextInput
          style={styles.textInput}
          onChangeText={placeHandler}
          value={placeValue}
        />
      </View>
      <View>
        <ImagePicker onImageTaken={imageTakenHandler} />
      </View>
      {/* <View style={{ width: 10, marginBottom: 15 }}>
        <Button
          title="Save Image"
          color={Colors.primaryColor}
          onPress={saveImageHandler}
        />
      </View> */}
      <View
        style={{
          // width: 150,
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
        }}
      >
        <Button
          color={Colors.primaryColor}
          title="ImageList"
          onPress={() => props.navigation.navigate("ImageList")}
        />
        <Button
          title="Upload Image"
          color={Colors.primaryColor}
          onPress={saveImageHandler}
        />
      </View>
    </ScrollView>
  );
};

CaptureImage.navigationOptions = (navigationData) => {
  return {
    headerTitle: "Capture Images",
  };
};

const styles = StyleSheet.create({
  form: {
    margin: 30,
  },
  label: {
    fontSize: 18,
    marginBottom: 15,
  },
  textInput: {
    borderBottomColor: "#ccc",
    borderBottomWidth: 1,
    marginBottom: 15,
    paddingVertical: 4,
    paddingHorizontal: 2,
  },
});

export default CaptureImage;
