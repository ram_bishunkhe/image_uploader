class CapturedImages {
  constructor(id, title, place, capturedImg) {
    this.id = id;
    this.title = title;
    this.place = place;
    this.capturedImg = capturedImg;
  }
}

export default CapturedImages;
